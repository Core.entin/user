## Installing /e/ from custom recovery

1. Download the /e/ install package that you’d like to install from [here](https://images.ecloud.global/dev/{{ device.codename }}/).
1. If you aren’t already in recovery mode, reboot into recovery mode:

    ```shell
    adb reboot recovery
    ```
1. *Optional* Tap the Backup button to create a backup. Make sure the backup is created in the external sdcard or copy it onto your computer as the internal storage will be formatted.
1. Go back to return to main menu, then tap Wipe.
1. Now tap Format Data and continue with the formatting process. This will remove encryption as well as delete all files stored on the internal storage.
1. Return to the previous menu and tap Advanced Wipe.
1. Select the Cache and System partitions to be wiped and then Swipe to Wipe.
1. Sideload the /e/ .zip package:
    - On the device, select “Advanced”, “ADB Sideload”, then swipe to begin sideload.
    - On the host machine, sideload the package using:

        ```
        adb sideload filename.zip
        ```
1. Once installation has finished, return to the main menu, tap Reboot, and then System
