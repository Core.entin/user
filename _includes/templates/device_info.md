{% assign device = site.data.devices[page.device] %}
<section class="container">
    <div class="left">
    {% include device_left.html %}
    </div>
    <div class="right">
    {% include device_right.html %}
    </div>
</section>
