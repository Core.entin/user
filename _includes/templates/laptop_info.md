{% assign laptop = site.data.laptops[page.laptop] %}
<section class="container">
    <div class="left">
    {% include laptop_left.html %}
    </div>
    <div class="right">
    {% include laptop_right.html %}
    </div>
</section>
