## Unlocking the bootloader

{% include alerts/warning_unlocking_bootloader.md %}

{% unless device.no_oem_unlock_switch %}

1. Enable OEM unlock in the Developer options under device Settings, if present.
{% endunless %}

1. Connect the device to your PC via USB.

1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:

    ```shell
    adb reboot bootloader
    ```

1. Once the device is in fastboot mode, verify your PC finds it by typing:

    ```shell
    fastboot devices
    ```

1. Now type the following command to unlock the bootloader:

    {% if device.custom_unlock_cmd %}
    ```
    {{ device.custom_unlock_cmd }}
    ```
    {% else %}
    ```
    fastboot oem unlock
    ```
    {% endif %}

1. Alternately on some devices to unlock the bootloader you may need to type the command:

    ```
    fastboot oem unlock-go    
    ```

1. Check if the device has been unlocked by typing the following command in command prompt (on Windows) or terminal (on Linux or macOS) window. The output will display a message similar to 'Device unlocked: true':

    ```
    fastboot oem device-info    
    ```
  


1. If the device doesn’t automatically reboot, reboot it. It should now be unlocked.

1. Since the device resets completely, you will need to re-enable USB debugging to continue.

{% if device.is_ab_device %}
{% include templates/recovery_install_fastboot_ab.md %}
{% else %}
{% include templates/recovery_install_fastboot_generic.md %}
{% endif %}
