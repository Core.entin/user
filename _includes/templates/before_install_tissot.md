## Important note

On some Xiaomi Mi A1 devices, you may encounter issues with the touchscreen if you follow this procedure only. If you follow this procedure and get a "crazy touchscreen" problem, please follow instructions listed [here](crazy-touchscreen) first and then restart the procedure from the next step.
