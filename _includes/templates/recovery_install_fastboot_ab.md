{%- assign device = site.data.devices[page.device] -%}
{% if device.custom_recovery_codename %}
{% assign custom_recovery_codename = device.custom_recovery_codename %}
{% else %}
{% assign custom_recovery_codename = device.codename %}
{% endif %}

## Temporarily booting a custom recovery using `fastboot`

1. Download a custom recovery - you can download [TWRP](https://dl.twrp.me/{{ custom_recovery_codename }}).
{% if device.alternate_twrp %}
> An [alternate location]({{ device.alternate_twrp }}) to download TWRP

{% endif %}
{% if device.twrp_version %}
  > The version of TWRP we used to test the ROM was {{ device.twrp_version }}

{% endif %}
1. Connect your device to your PC via USB.
1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:
    ```
    adb reboot bootloader
    ```
1. Once the device is in fastboot mode, verify your PC finds it by typing:
    ```
    fastboot devices
    ```
{% if device.fastboot_boot %}
1. Temporarily boot a recovery on your device by typing

    ```
    fastboot boot twrp-x.x.x-x-{{ device.codename}}.img
    ```
{% else %}
1. Temporarily flash a recovery on your device by typing:
    ```
    fastboot flash boot <recovery_filename>.img
    ```

1. Manually reboot into recovery mode
{% endif %}
