<strong>Requirements</strong>

1. Highly recommended: it is recommended that you have an /e/ account (like: john.doe@e.email) if you want to benefit from /e/ account integration for all online services such as: email, drive, calendar, notes, tasks. [Register for your free /e/ account](../../../create-an-ecloud-account).
1. Make sure your computer has working `adb` and `fastboot`. Setup instructions can be found [here](https://wiki.lineageos.org/adb_fastboot_guide.html).
1. Enable USB debugging on your device.
1. Please read through the instructions at least once completely before actually following them to avoid any problems because you missed something!
