---
layout: page
permalink: /pages/install-adb
search: exclude
---
## Steps to Install adb on your PC
  * Get[ SDK Platform tools](https://developer.android.com/studio/releases/platform-tools) package specific to your PC OS
  * Check the installation instructions and install dependencies as per the details on the site.
     > Please note: Windows users will need the proper drivers installed on their computer


### For Ubuntu users:

  *  Open a terminal window and type:
  ```
  sudo apt update
  sudo apt install android-tools-adb android-tools-fastboot
  ```
### To check ADB version type:
  ```
  adb version
  ```
### Sample output:
  ```
  Android Debug Bridge version 1.0.36
  ```
### Set File transfer mode on the device
* On the phone you would need to change the default USB Preferences

  Steps to do this are
* Swipe down on the phone once it is connected via USB to an adb enabled PC
* You would see an option 'Android system: Charging this device via USB'
> This is the default mode when connected via USB

* Tap on the option to display other modes available
* Select 'File Transfer mode'
* Come out of the screen
