---
sidebar: home_sidebar
title: Info about PineBook Laptops
folder: info
layout: default
permalink: /devices/pinebook
device: pinebook
---
{% include templates/laptop_info.md %}
