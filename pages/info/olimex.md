---
sidebar: home_sidebar
title: Info about Olimex Laptops
folder: info
layout: default
permalink: /devices/olimex
device: olimex
---
{% include templates/laptop_info.md %}
