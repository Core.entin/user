---
layout: page
title: Crazy touchscreen issue for Xiaomi Mi A1 - tissot
permalink: devices/tissot/crazy-touchscreen
---

This procedure has been tested to fix the "crazy touchscreen" issue on Xiaomi Mi A1.

> **Very important: please read the following carefully before proceeding!**
>
> Installing a new operating system on a mobile device can potentially:
> 1. lead to all data destruction on the device
> 1. make it an unrecoverable brick.
>
> So please only flash your device if you know what you are doing and are OK with taking the associated risk.
>
> The /e/ project and its project members deny any and all responsibility about the consequences of using /e/ > software and/or /e/ services.

## Installing /e/ coming from MiUI 10 beta (based on Android 9.0 Pie)

> Note: The following procedure assumes you have already unlocked your device's bootloader.

> Warning: The following procedure WILL delete all data on your phone (including files stored on the internal storage). Please do not hesitate to backup your photos, videos, SMS, call-logs, etc. to an external drive or PC! Files stored on the external SD card (if you have inserted one into your device) will NOT be deleted.

> Author's advice: For the following procedure, make sure you are willing to use ~ 2.4 GB of your monthly internet quota.

1. Download [Xiaomi's official](https://in.c.mi.com/thread-235054-1-0.html) "XiaoMiFlash" application and follow the installation instructions.
1. Once installed, download [XiaomiFirmware](https://xiaomifirmware.com/roms/download-official-roms-xiaomi-mi-a1/) latest Nougat ROM such as "Android 7.1.2 Nougat (N2G47H.7.12.19)".
1. Once downloaded, un-zip the TGZ file. Then, un-tar the TAR file.
1. Connect your device to your PC over USB. Then, run the following command:

   ```shell
   adb reboot bootloader
   ```

1. On your PC, launch "XiaoMiFlash" and select the FOLDER you un-zipped above.
1. In the bottom right corner, select "clean all" and then click "flash".

    > Author's advice: Get yourself something to drink while you wait for the flash to complete.

1. Once the flash is complete, your device should boot into AndroidOne (based on Android 7.12). Complete the setup wizard (you can skip setting up your accounts, fingerprint, etc.) and enable `USB debugging` under `Developer options`.

1. Now run the following command:

   ```shell
   adb reboot bootloader
   ```

1. OEM unlock

   ```
   fastboot oem unlock
   ```

1. Flash this custom version of [TWRP recovery-Oreo](https://androidfilehost.com/?fid=818070582850498337):

   ```shell
   fastboot boot /tmp/recovery-3.2.1-2-oreo.img
   ```

    Wait more than 10 sec for the device to automatically reboot in TWRP (recovery) mode.

1. Once in recovery mode, copy [this stock Oreo ROM](https://drive.google.com/drive/folders/1f7o_UnThNgVAOKZYLjprAf65C3YJhDtq) to the device:

   ```shell
   adb push 9.6.4.0v8.1.zip /sdcard/
   ```

1. In TWRP select tap install, select the file that was copied, and complete the zip installation

1. Once completed press reboot

1. Wait for the boot to complete, enable developer mode, enable USB debugging.

Now you can go back to the previous instructions and continue from "Installing a custom recovery": this time, use the TWRP version listed in the documentation, not the custom one from this page.
