---
layout: page
title: Install Heimdall
permalink: how-tos/install-heimdall
---

The Heimdall project does not provide up to date packages. This means that in some cases, it will just not work. Consequently, we provide [patched sources](https://gitlab.e.foundation/e/tools/heimdall](https://gitlab.e.foundation/e/tools/heimdall) and corresponding binaries.

> If you prefer to build the Heimdall suite by yourself, you can follow our documentation available [here](build-heimdall)

## Download Heimdall binaries

Download binaries provided by us [here](https://images.ecloud.global/tools/heimdall/) for you platform.

## Install

### On Windows

At the moment, the Windows build is not available. Please build it by yourself following [our documentation](build-heimdall).

### On Ubuntu

Run the following commands to install heimdall for all users:

```
$ unzip /path/to/heimdall_ubuntu.zip -d /tmp
$ cp /tmp/bin/heimdall* /usr/bin/
$ rm -rf /tmp/bin
```

### On MacOSX

Run the following commands to install heimdall for all users:

```
$ unzip /path/to/heimdall_macos.zip -d /tmp
$ cp /tmp/bin/heimdall /usr/local/bin
$ rm -rf /tmp/bin
```

## Validate

In a terminal, you can run run the following command to check that everything is working properly:

```
$ heimdall info
Heimdall v1.4.2

Copyright (c) 2010-2017 Benjamin Dobell, Glass Echidna
http://www.glassechidna.com.au/

This software is provided free of charge. Copying and redistribution is
encouraged.

If you appreciate this software and you would like to support future
development please consider donating:
http://www.glassechidna.com.au/donate/

Heimdall utilises libusbx for all USB communication:
    http://www.libusb.org/

libusbx is licensed under the LGPL-2.1:
    http://www.gnu.org/licenses/licenses.html#LGPL
```
