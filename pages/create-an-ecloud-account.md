---
layout: page
title: Create an /e/ account
permalink: create-an-ecloud-account
search: exclude
toc: true
---

### Purpose of an /e/ account

* having a new brand email address @e.email
* having a single identity to access all /e/ services :

   - /e/ Email : username@e.email
   - /e/ Drive : photos, videos, documents...
   - /e/ Calendar
   - /e/ Contacts
   - /e/ Notes
   - /e/ Tasks

* in your /e/ smartphone and online at : [https://ecloud.global](https://ecloud.global)

### How to get your free /e/ account ?

* Go to [https://e.foundation/e-mail-invite/](https://e.foundation/e-mail-invite/) you should see a screen like this

![](images/e-email.png)

* And follow the below steps :
	 - enter your current email address
	 - wait for an invitation in your mailbox ! Usually it takes < 2 minutes. Check your SPAM box !

### Get Started

* After you clicked the link in the invitation email you can proceed to the actual account creation :

![](images/create-e-account.png)

### Creating your own /e/ account @e.email

* Fill the fields correctly ! This example will create an /e/ identity as John Doe <john.doe@e.email>
* Do not use special characters in your username or password
* Do not use a combination of uppercase and lower case alphabets in your username

![](images/your-e-id.png)

### Using an /e/ account @e.email

* Now that your /e/ account is created, you can use it in your /e/ smartphone 
  - In the first time usage wizard just after /e/ OS installation
  - You can add account by going to..
     ````
     Settings >> Accounts >> Add account >> /e/
     ````

* On the web at [https://ecloud.global](https://ecloud.global) to retrieve all the pictures, videos you took with your /e/ smartphone
* To access on the [ecloud](https://ecloud.global) all you emails and check your calendar

### Having trouble creating an /e/ account @e.email?

* If you never received the invitation on your current email, check your email client SPAM folders
* If you cannot login in your /e/ smartphone or at [https://ecloud.global](https://ecloud.global) try to reset your password with the reset link available at [https://ecloud.global](https://ecloud.global)
* Make sure that you use your username@e.email as email for the reset process, NOT your recovery email.
* If that does not work for you contact us at <contact@e.email>

### How can I delete my /e/ account @e.email?

* Send in an email to <contact@e.email> from the /e/ID you want to delete
* This is to confirm the ID is yours
* Mention the detail that you want to delete the ID in the email
* Once we receive the email, we will delete the ID from our servers.
  >  We are working on making this process better and user-driven


### Some more tips on your /e/ account @e.email

* Your /e/ identity at ecloud.global is not (yet) compatible with our various websites and forums.

* This means that you will have to create specific login/password at [community.e.foundation](https://community.e.foundation/), [gitlab.e.foundation](https://gitlab.e.foundation/e), and [e.foundation (shop)](https://e.foundation/e-pre-installed-refurbished-smartphones/)
* Do not try to login in your /e/ smartphone with your community or gitlab identity. It will not work.
* Your /e/ identity is under this form :
  ***username@e.email***

* This is your email address too. Yes. « .email » is a valid extension on internet, like .com, .org, .xyz and thousands others.

* And forget username@e.email.org we don't own the email.org domain.


### Important note
* Your free account will be **limited to 5GB** online storage. We now have some premium storage plans available. You can also **upgrade it to 20GB** while supporting the /e/ project by becoming an [/e/ Early Adopter](https://e.foundation/donate/).

**PLEASE KEEP IN MIND** that /e/ Free drive and mail accounts are supported by donations! Please [support us now and receive a gift](https://e.foundation/support-us/).

<i>"Create /e/ free account" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018-2019.</i>
