---
layout: page
title: Development process
permalink: internal/development-process
search: exclude
---

## 1. Prepare sprint

- [Issue triage](prepare/issue-triage)
- [Priorisation](prepare/priorisation)
- [Planification](prepare/planification)
- [Sprint](prepare/sprint)

## 2. Development

- [Issue assignment](dev/issue-assignment)
- [Development](dev/development)
- [Validation](dev/validation)

## 3. Test session

- [Run test sprint](test/run-test-build)
- [Test session](test/test-session)

## 4. Release sprint

- [Prepare code for release](release/prepare-code-for-release)
- [Write release note](release/release-note)
- [Annoucement](release/announcement)
- [Closing the sprint](release/closing-the-sprint)

## Other

- MR template
- issue template
- branch management
- sprint name convention
