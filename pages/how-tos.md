---
layout: page
title: How Tos
permalink: how-tos/
search: exclude
---
You can find links to the complete set of HOWTO's available on the /e/ community forum on this page

## Applications and Settings
- [Installing Banking Apps](https://community.e.foundation/t/howto-installing-banking-apps/5875/9)
- [Using WhatsApp under shelter](https://community.e.foundation/t/howto-using-whatsapp-under-shelter/5662)
- [Tor-browser on /e/](https://community.e.foundation/t/howto-tor-browser-on-e/2413)
- [Use the AppStore](https://community.e.foundation/t/howto-use-the-appstore/4142)
- [How to deal with unstable applications?](https://community.e.foundation/t/howto-how-to-deal-with-unstable-applications/3970)
- [Place a widget on BlissLauncher](https://community.e.foundation/t/howto-place-a-widget-on-blisslauncher/3793)
- [Disable finger sensor on Redmi Note 4](https://community.e.foundation/t/howto-disable-finger-sensor-on-redmi-note-4/700)
- [Report issues in Applications in /e/](https://community.e.foundation/t/howto-report-issues-in-applications-in-e/2993)
- [Phone not switching from 2G to higher – workaround](https://community.e.foundation/t/howto-phone-not-switching-from-2g-to-higer-workaround/646)
- [Get a “/e/ user” title on this forum](https://community.e.foundation/t/howto-get-a-e-user-title-on-this-forum/1024)
- [Install F-Droid App](https://community.e.foundation/t/howto-install-f-droid-app/432)
- [Report an issue](report-an-issue)
- [Factory reset](factory-reset)


## How to Build /e/
- [Build /e/](build-e)
- [Building unofficial /e/ rom on a cloud VM - various options](https://community.e.foundation/t/howto-building-unofficial-e-rom-on-a-cloud-vm-various-options/6877)
- [First steps if your build won’t boot](https://community.e.foundation/t/how-to-first-steps-if-your-build-wont-boot/5480)
- [Find files in your source tree](https://community.e.foundation/t/howto-find-files-in-your-source-tree/4794)
- [Get your device code using adb](https://community.e.foundation/t/howto-get-your-device-code-using-adb/664)
- [Get a log from your phone](create-a-log)
- [Some useful Docker commands](https://community.e.foundation/t/howto-some-useful-docker-commands/3075)
- [Installing ADB and Fastboot on your computer](https://community.e.foundation/t/howto-installing-adb-and-fastboot-on-your-computer/3051)
- [What is ADB? (Android Debug Bridge)](https://community.e.foundation/t/howto-what-is-adb-android-debug-bridge/3057)
- [Android bootloader, fastboot, recovery and normal booting](https://community.e.foundation/t/howto-android-bootloader-fastboot-recovery-and-normal-booting/3052)
- [Unlock bootloader](https://community.e.foundation/t/howto-unlock-bootloader/3053)
- [How much space do I need to build for /e/?](https://community.e.foundation/t/howto-how-much-space-do-i-need-to-build-for-e/3014)

## /e/ ROM and supporting tools Installation
- [Install Heimdall for Samsung Smartphones](install-heimdall)
- [Install /e/ on a Samsung smartphone with Windows easily](https://community.e.foundation/t/howto-install-e-on-a-samsung-smartphone-with-windows-easily/3186)
- [Update /e/](https://community.e.foundation/t/howto-update-e/3222)
- [Enable adb logcat on first boot without authentication (debugging boot loops)](https://community.e.foundation/t/howto-enable-adb-logcat-on-first-boot-without-authentication-debugging-boot-loops/4496)
- [Install /e/ for my device](https://community.e.foundation/t/howto-install-e-for-my-device/2915)
- [Install TWRP](https://community.e.foundation/t/howto-install-twrp/3056)
- [Enable developer options and ADB/USB debugging](https://community.e.foundation/t/howto-enable-developer-options-and-adb-usb-debugging/3055)
- [Find device code](https://community.e.foundation/t/howto-find-device-code/3066)
- [Find out if my device is supported by /e/](../devices)
- [Find if there is a Nougat or Oreo build for my device](https://community.e.foundation/t/howto-find-out-if-my-device-is-supported-by-e/2889)

## Configuring Mail & Contacts
- [Mail app sync issues – workaround](https://community.e.foundation/t/howto-mail-app-sync-issues-workaround/647)
- [Import contacts from Apple to /e/ cloud](https://community.e.foundation/t/howto-import-contacts-from-apple-to-e-cloud/5532)
- [Sync your contacts, calendar events, email, notes and tasks to /e/](https://community.e.foundation/t/howto-sync-your-contacts-calendar-events-email-notes-and-tasks-to-e/3263)
- [Access /e/-mailserver with Thunderbird](https://community.e.foundation/t/howto-access-e-mailserver-with-thunderbird/3154)
- [Access my /e/ contacts, calendar, email and files in a browser](https://community.e.foundation/t/howto-access-my-e-contacts-calendar-email-and-files-in-a-browser/2953)
- [Email Configurations - to configure /e/ on most email clients](https://community.e.foundation/t/howto-email-configurations-to-configure-e-on-most-email-clients/632)
- [Access my /e/ email’s in Thunderbird](https://community.e.foundation/t/howto-access-my-e-emails-in-thunderbird/2962)
- [Is my /e/ email ID a test ID / temporary ID?](https://community.e.foundation/t/howto-is-my-e-email-id-a-test-id-temporary-id/2952)
- [Get an /e/ email ID](../create-an-ecloud-account)
- [Import Gmail contacts into eCloud](https://community.e.foundation/t/howto-import-gmail-contacts-into-ecloud/2809)
- [Add a gmail ID from the mail app](https://community.e.foundation/t/howto-add-a-gmail-id-from-the-mail-app/2807)

## How to Support /e/
- [Be part of /e/](https://e.foundation/get-involved/)
- [Crowdfunding to fuel the /e/ adventure!](https://e.foundation/donate/)
