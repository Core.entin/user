---
layout: page
title: Jobs @/e/
permalink: jobs
search: exclude
---

## Currently opened positions (remote):

Those are full time positions. 

 - [WEBMASTER/WORDPRESS] we are looking for webmaster with significant experience working with wordpress and woocommerce on a multilingual and high traffic website. Mid-level in PHP programming would be appreciated.
 - [DESIGNER/GRAPHIST] we are looking for a part time designer/graphist to work on our mobile operating system User Interface and experience. 
 - [PHP] we are looking for PHP developer with experience, to work on various online services components, improve them, add some features, debug... Passionate, hacker in mind, happy to work as part of a multi-cultural team, for a great project like /e/!
 
## How to apply?

Please send an email to <jobs@e.email> with a resume.
