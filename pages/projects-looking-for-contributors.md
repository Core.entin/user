---
layout: page
title: Projects looking for contributors
permalink: projects-looking-for-contributors
search: exclude
toc: true
---

All /e/ projects welcome contributors! Look for your preferred project and register for user [on this website](https://gitlab.e.foundation) to contribute!

## Open Positions at /e/

Those are full time remote positions:

- [WEBMASTER/WORDPRESS] we are looking for webmaster with significant experience working with wordpress and woocommerce on a multilingual and high traffic website. Mid-level in PHP programming would be appreciated.
- [DESIGNER/GRAPHIST] we are looking for a part time designer/graphist to work on our mobile operating system User Interface and experience.
- [PHP] we are looking for PHP developer with experience, to work on various online services components, improve them, add some features, debug… Passionate, hacker in mind, happy to work as part of a multi-cultural team, for a great project like /e/!

### How to apply?

Please send an email to <jobs@e.email> with a resume.

## Specific projects looking for contributors

We have several projects that need contributors:

* Help fixing issues! Reporting issues is great for /e/, fixing them **deserves our eternal recognition**. Want to [pick an issue and work on it](https://gitlab.e.foundation/groups/e/-/issues)?

* ROM developer/maintainer for Qualcomm (Snapdragon) and Mediatek chipsets: we want to provide support for newest devices available on the market. If you are interested in working on this, please [contact us](#how-to-contact-us).

* OpenCamera: some issues have been detected on some devices, probably related to some hardware drivers/firmware. For instance, OpenCamera is randomly crashing on LeEco Le2 and probably other devices, depending on some settings. Also, we'd like to have the best ever open-source camera application in term of usability and picture result. If interested in this project please contact us, and we will start a fork (+ contribute upstream). [contact us](#how-to-contact-us)!

* [/e/ TWRP](https://gitlab.e.foundation//e/tools/etwrp): we'd like to have a customized version of TWRP to make the system update UX smoother. C/Android programmer here! You can [contribute](https://gitlab.e.foundation//e/tools/etwrp) to and/or test what has been started here. [Contact us](#how-to-contact-us)

* Personal assistant: this is going to be a key project for /e/: we need a personal assistant that respects your privacy. The project has not really been started but we have some ideas and a general roadmap. We need ASR specialists, conversational models specialists, AI specialists (deep learning/neural networks). If interested in joining, please [contact us](#how-to-contact-us) about it.

## Major /e/OS-related projects looking for contributors

/e/OS is using, modifying, integrating a lot of different open source software components, including those ones that are key for /e/OS:

* LineageOS: /e/OS is forking LineageOS for different needs, different users. Therefore, better hardware support for LineageOS means a better hardware support for /e/OS. [Contribute to LineageOS](https://lineageos.org/)!

* Exodus Privacy: Exodus Privacy is a non-profit organization that creates software tools to inform people about privacy issues in Android applications, such as abusive permissions & trackers. They [welcome software development and financial](https://exodus-privacy.eu.org/) contributions!

* Help the [microG project](https://microg.org/)! microG is helping us to get rid of the Google services. It needs good developers to help and accelerate the development and fix issues. Looking for a first developer challenge with microG? Help [fixing this issue](https://github.com/microg/android_packages_apps_GmsCore/issues/562)! Join the microG project [on GitHub](https://github.com/microg).

* NextCloud: several /e/OS online services heavily rely on NextCloud (a fork of OwnCloud), like cloud storage, online calendar, notes etc. [Contribute to NextCloud](https://nextcloud.com/contribute/)!

* Searx: searx is a great open source meta-search engine that we are forking to make our default search engine ([spot.ecloud.global](https://spot.ecloud.global)). [Contribute to Searx](https://asciimoo.github.io/searx/)!

## Is it paid?

Projects here are open source projects and we know that many people really appreciate voluntary contributions to open source projects, for the benefit of all.

However, if you think, as a specialist, that the project will require a huge amount of work and that you are ready to make it within a limited timeframe, we can consider a bounty or a paid-project. Please contact us.

## How to contact us?

Please send an email to <join@e.email>, with a clear subject line (like: ROM Developer Mediatek).

Tell us why you are interested in participating, how your skills are related to this project, and any other useful information.
