---
layout: page
title: How to use Weblate
permalink: how-to-use-weblate
search: exclude
toc: true
---
Before we start explaining how to use Weblate lets answer a few basic questions that you might have.
### What is Weblate?
Weblate is a libre web-based translation tool with tight version control integration. It provides two user interfaces, propagation of translations across components, quality checks and automatic linking to source files. You can get more details about the software [here](https://weblate.org/en/)

### Why do we use Weblate in /e/OS
We use Weblate to automate the process of localization of all default Application text.

### What languages are supported on /e/OS

For now we plan to start with these languages

<ul>
{% for lang in site.data.languages %}
    <li>
      {{ lang.language }}
    </li>
{% endfor %}
</ul>

>Please note: The translations into languages other than English are in progress

### Are these the only languages that will be supported?
Want to help translate the text in /e/OS in your own language write to us on <contact@e.email>

### Can I help translate /e/OS on Weblate?

- Yes, anyone can help with the translations
- All you need is a [/e/Gitlab ID](https://gitlab.e.foundation)

### Ok I have my /e/Gitlab ID how do I proceed ?

- Log in to [https://i18n.e.foundation/](https://i18n.e.foundation/)  with your /e/Gitlab ID - This is how the login screen for weblate would look
![Login page](images/Weblat_login.png)

- Once you have succesfully logged in  you should see a screen similar to this
![Weblate Dashboard](images/Weblate_Dashboard.png)

### How to use weblate and translate

- This is the Weblate Dashboard
- On the menu **>> Projects >> Browse all Projects**
- This should bring you to this screen

![Browse all projects](images/Weblate_Browse_All_Projects.png)

- Select /e/
- This would open the following screen

![Weblate component view](images/Weblate_Components_View.png)

> Note: In Weblate projects are called Components

- Select the Component you have to work on for e.g. Bliss Launcher or Account Manager
- This will open a screen which will show you the different languages in which we translate
- The next screen is where you would be working
- In the Box on top marked 'Translate' you would see words in English in one text box below you will see another text box where you type the translation in the language you are working on.
![Translate](images/Weblate_Translate.png)
- After each word has been entered click 'Suggest'
- The word will now show up in the list below with a small black icon next to it
![Suggestion](images/Weblate_Suggestion.png)

- The next word will show up for you to Translate
- Please note the 'Save' button will be disabled. This is because there will be a review of these words and then they will be submitted.

### Why am I not able to Save directly ?

- We have opened up the /e/ Weblate url to all users.
- This will result in multiple users sharing or working on the same language
- There can be multiple suggestion or duplicates for a single word
- To resolve this we will review the suggestions
- Translors can also vote on the suggestions that best translate the word.
- The suggestions with most votes will be accepted.

### Still have questions about Weblate and how it works?

- You can check the [documentation on the Weblate site](https://docs.weblate.org/en/weblate-3.10.3/index.html)
