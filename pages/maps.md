---
layout: page
title: Maps application
permalink: maps
search: exclude
---

Regarding the Maps application: it's the only application that is not (yet) open source in /e/. We have tested many Maps applications, and the only app that can be compared to the common well-known maps application is "[Magic Earth](https://www.magicearth.com/)".

We have discussed with the publishers of this application and while they have not decided yet to go open source, they have provided us documentation about the privacy behavior of this application. My personal feeling is that if we show enough adoption for this application, it will eventually turn open source.

## Privacy statement from Magic Earth publisher General Magic

Privacy means Freedom.
That’s why we do not collect or share personal information.

The applied rules for sending and storing data are the following:
1. only transmit necessary, so minimal information from the user to the server
2. where possible, anonymise data already on the phone before sending
3. if pseudonymous info is needed (for example for debugging), only keep it on the server pseudonymously for an as short period as possible
4. do not create user profiles
5. do not share personal information

In the used eco system, NO external service api’s are used to prevent private data leakage.

For details, see [User Data - Privacy_20180906_v06.pdf](/pdf/user_data_privacy_20180906_v6.1.pdf).
