---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/kiwi/install
device: kiwi
---
{% include templates/device_install.md %}
