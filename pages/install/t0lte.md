---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/t0lte/install
device: t0lte
---
{% include templates/device_install.md %}
