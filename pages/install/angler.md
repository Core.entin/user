---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/angler/install
device: angler
---
{% include templates/device_install.md %}
