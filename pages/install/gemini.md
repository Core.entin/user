---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/gemini/install
device: gemini
---
{% include templates/device_install.md %}
