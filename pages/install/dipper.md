---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/dipper/install
device: dipper
---
{% include templates/device_install.md %}
