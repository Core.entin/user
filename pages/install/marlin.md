---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/marlin/install
device: marlin
---
{% include templates/device_install.md %}
