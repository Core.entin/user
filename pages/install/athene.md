---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/athene/install
device: athene
---
{% include templates/device_install.md %}
