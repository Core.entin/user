---
sidebar: home_sidebar

folder: install
layout: page
permalink: /devices/bacon/install
device: bacon
---
{% include templates/device_install.md %}
