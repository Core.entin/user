---
sidebar: home_sidebar
title: Build /e/ for Motorola Moto G 4G - peregrine
folder: build
layout: page
permalink: /devices/peregrine/build
device: peregrine
---
{% include templates/device_build.md %}
