---
sidebar: home_sidebar
title: Build /e/ for Google Pixel XL - marlin
folder: build
layout: page
permalink: /devices/marlin/build
device: marlin
---
{% include templates/device_build.md %}
