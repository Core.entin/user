---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy J7 (Exynos) - j7eltexx
folder: build
layout: page
permalink: /devices/j7eltexx/build
device: j7eltexx
---
{% include templates/device_build.md %}
