---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy Note 3 (International 3G) - ha3g
folder: build
layout: page
permalink: /devices/ha3g/build
device: ha3g
---
{% include templates/device_build.md %}
