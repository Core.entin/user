---
sidebar: home_sidebar
title: Build /e/ for OnePlus X - onyx
folder: build
layout: page
permalink: /devices/onyx/build
device: onyx
---
{% include templates/device_build.md %}
