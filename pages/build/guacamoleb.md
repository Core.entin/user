---
sidebar: home_sidebar
title: Build /e/ for OnePlus 7 - guacamoleb
folder: build
layout: page
permalink: /devices/guacamoleb/build
device: guacamoleb
---
{% include templates/device_build.md %}
