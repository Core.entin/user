---
sidebar: home_sidebar
title: Build /e/ for FairPhone FP2 - FP2
folder: build
layout: page
permalink: /devices/FP2/build
device: FP2
---
{% include templates/device_build.md %}
