---
sidebar: home_sidebar
title: Build /e/ for Xiaomi Mi 8 - dipper
folder: build
layout: page
permalink: /devices/dipper/build
device: dipper
---
{% include templates/device_build.md %}
