---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S6 - zerofltexx
folder: build
layout: page
permalink: /devices/zerofltexx/build
device: zerofltexx
---
{% include templates/device_build.md %}
