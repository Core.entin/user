---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy A3 (2016 Exynos) - a3xeltexx
folder: build
layout: page
permalink: /devices/a3xeltexx/build
device: a3xeltexx
---
{% include templates/device_build.md %}
