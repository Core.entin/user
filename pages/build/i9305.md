---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S III (LTE/International) - i9305
folder: build
layout: page
permalink: /devices/i9305/build
device: i9305
---
{% include templates/device_build.md %}
