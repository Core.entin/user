---
layout: page
title: /e/ FAQ
permalink: faq
toc:  true
---

## Is /e/ open source?

Yes - all source code is available and you can compile it, fork it... Some prebuilt applications are used in the system; they are built separately from source code available here, or synced from open source repositories such as F-Droid. We ship one proprietary application though ([read the statement](/maps)).

### Cool, /e/ is open source - how can I contribute?

- join one of the /e/ [projects looking for contributors](/projects-looking-for-contributors)
- help solving [issues listed here](https://gitlab.e.foundation/groups/e/-/issues)
- [start building](build-e) /e/ for a new device and become a maintainer

Maybe you have other suggestions, feel free to contact us! Send an email to <join@e.email>, with a clear subject line (like: ROM Developer Mediatek).

### Is /e/ LineageOS + microG?

/e/ is forked from LineageOS. We've modified several parts of the system (and we're just beginning): installation procedure, settings organization, default settings. We've disabled and/or removed any software or services that were sending personal data to Google (for instance, the default search engine is no longer Google). We've integrated microG by default, have replaced some of the default applications, and modified others. We have added a synchronization background software service that syncs multimedia contents (pictures, videos, audio, files...) and settings to a cloud drive, when activated.

Also, we've replaced the LineageOS launcher with our own new launcher, written from scratch, that has a totally different look and feel from default LineageOS.

We've implemented several /e/ online services, with a single /e/ user identity (user@e.email). This infrastructure will be offered as docker images for self hosting: drive, email, calendar... to those who prefer self-hosting.

We have added an account manager within the system with support for the single identity. It allows users to log only once, with a simple "user@e.email" identity, for getting access to /e/'s various online services (drive, email, calendar, notes, tasks).

We now have a /e/ product description [here](what-s-e)

### Why should I use /e/? I can customize the ROM myself...

Absolutely! /e/ was not made for geeks or power users, it's designed to offer a credible and attractive alternative to the average user, with more freedom and a better respect of user's data privacy compared to mobile operating systems offered by the worldwide duopoly in place.

We really appreciate your involvement though. Your beta-testing and valuable feedback will help to make this alternative available to many more people who can't do this sort of thing for themselves.    

### I'm a geek and I love the default AOSP/LineageOS look

In that case, you probably won't be very happy with /e/, don't use it!

### Aren't you stealing the work of LineageOS developers?

No - we're using the rules of open source software. Just like AOSP-Android is forking the Linux kernel work, just like LineageOS is forking AOSP work, /e/ is forking LineageOS work. /e/s focus is on the final end-user experience, and less on the hardware. /e/OS and LineageOS's purposes and target users are different. However, we strongly encourage core developers to contribute upstream to LineageOS. When thinking about LineageOS vs /e/, think about Debian vs Ubuntu.

### Does /e/ have a simple way for updating the system regularly?

Yes - a simple 1-click (well, that may be 2 or 3!) "over the air" update feature is available in settings.

### Is my XYZ123 device supported?

Look at the [device list](/devices/). We are adding new devices regularly.

### From which version of Android is /e/ forked?

 - Android 7 (Nougat)/LOS14 (/e/-0.x-N branch)
 - Android 8 (Oreo)/LOS 15 (/e/-0.x-O branch).
 - Android 9 (Pie)/LOS 16 (/e/-0.x-P" branch).

### Is /e/ stable?

No - we're in beta stage at the moment. The system is pretty usable though, but use it at your own risk!

## Will you offer /e/-preloaded phones?

Yes - refurbished smartphones flashed with /e/OS, [are now available](https://e.foundation/e-pre-installed-smartphones/).

## If I find a bug, should I send you an email?

No - please report bugs and send suggestions using this platform as [explained here](/issues/). Please avoid using email or IM for reporting issues, it's not an efficient process.

## If I have suggestion, how should I contact you?

You can also use this platform to report suggestions. If you have suggestions related to improve privacy, you can also send an email to privacychallenge@e.email

## Why this weird, inconvenient /e/ name?

We had to change our name for legal reasons [as explained here](https://www.indidea.org/gael/blog/leaving-apple-and-google-e-is-the-symbol-for-my-data-is-my-data/).

/e/ is a textual representation of our "e" symbol which means "my data is MY data".

It's the current project codename, we will probably introduce a new and more convenient name for our mobile ROM in a few months.

## Where do the Apps in /e/ installer application come from?

Apps available in the /e/ Installer are open source apps that you can find on Fdroid, and free Android apps, same as you can find on  Google Play Store. The /e/ app installer relies on a third-party application repository called [cleanapk.org](https://info.cleanapk.org)

Soon, mobile web apps and progressive web apps will also be available through the /e/ application install.

### Where is the source code base of the /e/ installer application ?

You can check the code here https://gitlab.e.foundation/e/apps/apps

### How can I request an application to be added to the library?

Users can request  a specific app  to be listed by making a query directly in the /e/ application installer (Settings >Request App)

### What information is provided for each app within the /e/ application installer ?

At the moment, each app features a Privacy rating. In the future, user rating and energy score will also be available. We will also provide classic user ratings to help user make a better choice

Privacy ratings provide a list of trackers and requested permissions that come in as part of installing these apps (Note: those are computed by Exodus Privacy software). Energy rating will provide details of how much energy is consumed by the app to work. Please note  some of these features are currently under development.

### How can i make sure  apps in the installer are not tampered with but ‘original'

Apps are checked either using a PGP signature check, or a checksum.

In case you suspect an app  to be tampered, we thank you  to report it so we can look at the issue and take action.

### What should you do  if you don't find an app you would like to use in the /e/ app  installer ?

If you can't find an Android app you would like to use, you can first request it in the app installer : Settings >Request App

An option is also to look for a web alternative . For instance, several services  are available with  progressive web apps that can be used on the mobile.: Uber, Twitter, Starbucks, Some banks are alos offering web services as  an alternative to native apps. For  easier access, a shortcut to the web service can easily be added to the /e/ BlissLauncher, using the web browser.

Some users have also reported some successes using the Yalp Store and Aurora Store applications, though it requires to authorize third-party applications installation in /e/OS settings.

## My device is no longer supported by LineageOS. Will /e/ also stop support?

/e/ is not dependent on LineageOS in deciding which devices to support or stop supporting. We already have several devices which are not on the LineageOS supported list. We plan to port more devices to work with /e/ code.

### If my device is not supported by LineageOS will I still get the security patches on time?

Yes. We will download and apply the security patches for devices if they are available for download from the source.

## Is my data on the /e/ servers encrypted?

  - All user data is encrypted on the main block storage and in backups too.
  - True E2E (end to end) encryption is in our plans as a long-term feature.

## How can I delete my /e/ ID?

 - Send in an email to <contact@e.email> from the /e/ID you want to delete
 - This is to confirm the ID is yours
 - Mention the detail that you want to delete the ID in the email
 - Once we receive the email, we will delete the ID from our servers.
 > We are working on making this process better and user-driven

## Other questions

Some more technical questions & answers are available [in a specific FAQ](https://community.e.foundation/t/e-os-smartphones-faq/6108)
