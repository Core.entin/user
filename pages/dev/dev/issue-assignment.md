---
layout: page
title: Issue assignment
permalink: internal/development-process/dev/issue-assignment
search: exclude
---

## What ?

This topic discuss of the way for a developer to handle an issue.

## Why ?

The aim is to give some autonomy to developers.

## Who ?

| People | Role |
| ------ | ---- |
|        |      |

## How ?

1. Handle issues that are not assigned to anyone
2. Handle in priority issues from the project you are maintainer onto
3. Handle in priority the issues with the higher priority (P1 > P7)
4. Handle in issues in this order: bug, improvement, new feature

## Where ?

The column **To do** on the sprint board

https://gitlab.e.foundation/groups/e/-/boards/12?scope=all&utf8=%E2%9C%93&state=opened&milestone_title=\<milestone title\>

## When ?

A developer doesn't have any issue assigned to him/her.


---

[Next (Development) >](development)
