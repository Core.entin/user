---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: page
title: Welcome to /e/ Documentation!

---

## About the Project

> Discover more about project /e/

- [What's /e/? Read the Product Description!](/what-s-e)
- [Install /e/ mobile OS on a supported smartphone!](/devices)
- [Create a free ecloud account](/create-an-ecloud-account)
- [HOWTOs](/how-tos)
- [FAQ](/faq)
- [Share your /e/xperience!](/testimonies)


## Contribute to /e/

> Do you want to help the project?

- [Projects looking for contributors](projects-looking-for-contributors)
- [Openings at /e/](jobs)
- [Translators](/translators)
- [Testers](/testers)
- [Volunteers](/volunteers)

## Learn more about

> Information about the project or its applications

- [Maps application](/maps)
- [The different build types](/build-status)
- [Apps Installer FAQ](/apps)
- [e.foundation Legal Notice & Privacy](https://e.foundation/legal-notice-privacy/)

## Short Cuts

> Quick links to /e/ websites, forums and telegram channels

- [/e/ web site](https://e.foundation/)
- [/e/ Source Code](https://gitlab.e.foundation/e)
- [Need Help? Support?](https://e.foundation/contact/)
- [Report a bug](https://gitlab.e.foundation/groups/e/-/issues) in the /e/ ROM or apps
- [Add your Smartphone to the supported list](https://community.e.foundation/c/e-devices/request-a-device)
