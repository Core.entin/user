---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S6 edge - zeroltexx
folder: build
layout: default
permalink: /devices/zeroltexx/build
device: zeroltexx
---
{% include templates/device_build.md %}
