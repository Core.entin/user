---
sidebar: home_sidebar
title: Build /e/ for Essential Phone PH1 - mata
folder: build
layout: default
permalink: /devices/mata/build
device: mata
---
{% include templates/device_build.md %}
