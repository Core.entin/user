---
sidebar: home_sidebar
title: Build /e/ for OnePlus 6 - enchilada
folder: build
layout: default
permalink: /devices/enchilada/build
device: enchilada
---
{% include templates/device_build.md %}
