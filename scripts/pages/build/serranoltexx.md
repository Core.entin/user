---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S4 Mini (International LTE) - serranoltexx
folder: build
layout: default
permalink: /devices/serranoltexx/build
device: serranoltexx
---
{% include templates/device_build.md %}
