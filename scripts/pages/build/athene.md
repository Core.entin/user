---
sidebar: home_sidebar
title: Build /e/ for Motorola Moto G4 - athene
folder: build
layout: default
permalink: /devices/athene/build
device: athene
---
{% include templates/device_build.md %}
