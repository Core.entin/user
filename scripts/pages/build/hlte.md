---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy Note 3 LTE (N9005/P) - hlte
folder: build
layout: default
permalink: /devices/hlte/build
device: hlte
---
{% include templates/device_build.md %}
