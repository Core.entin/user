---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy Note 2 (3G) - n7100
folder: build
layout: default
permalink: /devices/n7100/build
device: n7100
---
{% include templates/device_build.md %}
