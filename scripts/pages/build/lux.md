---
sidebar: home_sidebar
title: Build /e/ for Motorola Moto X Play - lux
folder: build
layout: default
permalink: /devices/lux/build
device: lux
---
{% include templates/device_build.md %}
