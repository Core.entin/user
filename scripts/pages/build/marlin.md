---
sidebar: home_sidebar
title: Build /e/ for Google Pixel XL - marlin
folder: build
layout: default
permalink: /devices/marlin/build
device: marlin
---
{% include templates/device_build.md %}
