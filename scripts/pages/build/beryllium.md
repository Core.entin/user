---
sidebar: home_sidebar
title: Build /e/ for Xiaomi Pocophone F1 - beryllium
folder: build
layout: default
permalink: /devices/beryllium/build
device: beryllium
---
{% include templates/device_build.md %}
