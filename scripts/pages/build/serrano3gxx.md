---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S4 Mini (International 3G) - serrano3gxx
folder: build
layout: default
permalink: /devices/serrano3gxx/build
device: serrano3gxx
---
{% include templates/device_build.md %}
