---
sidebar: home_sidebar
title: Build /e/ for OnePlus 3/3T - oneplus3
folder: build
layout: default
permalink: /devices/oneplus3/build
device: oneplus3
---
{% include templates/device_build.md %}
