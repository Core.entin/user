---
sidebar: home_sidebar
title: Build /e/ for Motorola Moto G4 Play - harpia
folder: build
layout: default
permalink: /devices/harpia/build
device: harpia
---
{% include templates/device_build.md %}
