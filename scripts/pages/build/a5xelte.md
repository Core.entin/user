---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy A5 (2016) - a5xelte
folder: build
layout: default
permalink: /devices/a5xelte/build
device: a5xelte
---
{% include templates/device_build.md %}
