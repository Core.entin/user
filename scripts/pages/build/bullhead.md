---
sidebar: home_sidebar
title: Build /e/ for Google Nexus 5X - bullhead
folder: build
layout: default
permalink: /devices/bullhead/build
device: bullhead
---
{% include templates/device_build.md %}
