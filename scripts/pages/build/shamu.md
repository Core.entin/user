---
sidebar: home_sidebar
title: Build /e/ for Google Nexus 6 - shamu
folder: build
layout: default
permalink: /devices/shamu/build
device: shamu
---
{% include templates/device_build.md %}
