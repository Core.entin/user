---
sidebar: home_sidebar
title: Build /e/ for Xiaomi Redmi 3S/3X - land
folder: build
layout: default
permalink: /devices/land/build
device: land
---
{% include templates/device_build.md %}
