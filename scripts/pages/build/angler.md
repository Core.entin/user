---
sidebar: home_sidebar
title: Build /e/ for Google Nexus 6P - angler
folder: build
layout: default
permalink: /devices/angler/build
device: angler
---
{% include templates/device_build.md %}
