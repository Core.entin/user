---
sidebar: home_sidebar
title: Build /e/ for Xiaomi Mi MIX 2s - polaris
folder: build
layout: default
permalink: /devices/polaris/build
device: polaris
---
{% include templates/device_build.md %}
