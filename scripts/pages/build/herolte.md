---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S7 - herolte
folder: build
layout: default
permalink: /devices/herolte/build
device: herolte
---
{% include templates/device_build.md %}
