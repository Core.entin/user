---
sidebar: home_sidebar
title: Install /e/ on Essential Phone PH1 - mata
folder: install
layout: default
permalink: /devices/mata/install
device: mata
---
{% include templates/device_install.md %}
