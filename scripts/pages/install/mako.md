---
sidebar: home_sidebar
title: Install /e/ on Google Nexus 4 - mako
folder: install
layout: default
permalink: /devices/mako/install
device: mako
---
{% include templates/device_install.md %}
