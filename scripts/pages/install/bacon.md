---
sidebar: home_sidebar
title: Install /e/ on OnePlus One - bacon
folder: install
layout: default
permalink: /devices/bacon/install
device: bacon
---
{% include templates/device_install.md %}
