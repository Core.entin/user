---
sidebar: home_sidebar
title: Install /e/ on Xiaomi Mi 8 - dipper
folder: install
layout: default
permalink: /devices/dipper/install
device: dipper
---
{% include templates/device_install.md %}
