---
sidebar: home_sidebar
title: Install /e/ on Samsung Samsung Galaxy A7 (2017) - a7y17lte
folder: install
layout: default
permalink: /devices/a7y17lte/install
device: a7y17lte
---
{% include templates/device_install.md %}
