---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy A5 (2016) - a5xelte
folder: install
layout: default
permalink: /devices/a5xelte/install
device: a5xelte
---
{% include templates/device_install.md %}
