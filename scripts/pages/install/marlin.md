---
sidebar: home_sidebar
title: Install /e/ on Google Pixel XL - marlin
folder: install
layout: default
permalink: /devices/marlin/install
device: marlin
---
{% include templates/device_install.md %}
