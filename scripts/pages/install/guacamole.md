---
sidebar: home_sidebar
title: Install /e/ on OnePlus 7 Pro - guacamole
folder: install
layout: default
permalink: /devices/guacamole/install
device: guacamole
---
{% include templates/device_install.md %}
