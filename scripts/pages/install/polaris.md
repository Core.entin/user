---
sidebar: home_sidebar
title: Install /e/ on Xiaomi Mi MIX 2s - polaris
folder: install
layout: default
permalink: /devices/polaris/install
device: polaris
---
{% include templates/device_install.md %}
