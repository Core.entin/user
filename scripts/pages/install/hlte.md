---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy Note 3 LTE (N9005/P) - hlte
folder: install
layout: default
permalink: /devices/hlte/install
device: hlte
---
{% include templates/device_install.md %}
