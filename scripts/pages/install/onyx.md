---
sidebar: home_sidebar
title: Install /e/ on OnePlus X - onyx
folder: install
layout: default
permalink: /devices/onyx/install
device: onyx
---
{% include templates/device_install.md %}
